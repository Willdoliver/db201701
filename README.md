
# Estrutura b�sica do trabalho de CSB30 - Introdu��o A Bancos De Dados

Modifique este aquivo README.md com as informa��es adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usu�rio GitLab para cada integrante do grupo.

- Nome do integrante 1, RA do integrante 1, usu�rio GitLab do integrante 1
- Nome do integrante 2, RA do integrante 2, usu�rio GitLab do integrante 2
- Nome do integrante 3, RA do integrante 3, usu�rio GitLab do integrante 3

## Descri��o da aplica��o a ser desenvolvida 

Descreva aqui uma vis�o geral da aplica��o que ser� desenvolvida pelo grupo durante o semestre. **Este texto dever� ser escrito quando requisitado pelo professor.** O conte�do vai evoluir � medida em que o grupo avan�a com a implementa��o.
